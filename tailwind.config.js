/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        poppins: ['"Poppins"', "sans-serif"]
      },
      colors: {
        text: "#393F48",
        brand: "#EF5DA8"
      }
    },
  },
  plugins: [],
}
