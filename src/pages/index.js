import Link from "next/link";
import { Bars3Icon } from "@heroicons/react/24/solid";
import Github from "@/components/svgs/github";
import InstagramIcon from "@/components/svgs/instagram";
import TaskIcon from "@/components/svgs/task";
import BookIcon from "@/components/svgs/book";

const menus = [
  { href: "/", text: "home" },
  { href: "/about", text: "about" },
  { href: "/contact", text: "contact" }
]

const icons = [
  { href: "github.com/aansulaimann", icon: Github },
  { href: "instagram.com/aansulaimann", icon: InstagramIcon }
]

const projects = [
  { href: "github.com/aansulaimann", image: 'WebImage', name: "API Student with API KEY" },
  { href: "github.com/aansulaimann", image: 'WebImage', name: "API Student with API KEY" },
  { href: "github.com/aansulaimann", image: 'WebImage', name: "API Student with API KEY" },
  { href: "github.com/aansulaimann", image: 'WebImage', name: "API Student with API KEY" },
]

export default function Home() {
  return (
    <>
      <header className="fixed w-full">
        <div className="py-3 md:py-4 px-4 mx-auto flex flex-row items-center justify-between md:max-w-3xl xl:max-w-7xl">
          <div className="flex items-center space-x-6">
            <Link href={'/'}>
              <div className="flex py-2 md:px-3 space-x-2 md:space-x-4 items-center">
                <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" alt="" className="w-6 h-6 md:w-10 md:h-10 object-cover rounded-full" />

                <span className="font-semibold leading-[160%]">Aan</span>
              </div>
            </Link>

            <nav className="hidden md:block">
              <ul className="flex space-x-2">
                {
                  menus.map((item, index) => (
                    <li key={index} className="px-3 py-2 leading-[160%] text-sm">
                      <Link href={item.href}>{item.text}</Link>
                    </li>
                  ))
                }
              </ul>
            </nav>

          </div>
          <div className="flex items-center space-x-3">
            <div className="hidden lg:flex items-center space-x-6">
              {
                icons.map((icon, index) => (
                  <a key={index}>
                    <icon.icon />
                  </a>
                ))
              }
            </div>
            <button className="py-2 px-6 text-sm leading-[160%] border border-brand rounded-full">
              Download CV
            </button>
            <button className="md:hidden lg:hidden p-2">
              <Bars3Icon className="h-6 w-6" />
            </button>
          </div>
        </div>
      </header>

      {/*  md:max-w-3xl xl:max-w-7xl md:py-4 md:px-4 px-6 py-12 */}

      <main className="mx-auto w-full py-7 px-4 md:py-12 md:max-w-3xl xl:max-w-7xl">
        {/* hero */}
        <section className="lg:flex lg:items-center lg:flex-row mt-[58px]">
          <div className="lg:basis-2/3">
            <h3 className="flex flex-col font-light text-[30px] md:text-[72px]">
              <span className="flex items-center">Let’s learn <TaskIcon className="md:w-14 md:h-14" /></span>
              <span className="flex items-center">to code <BookIcon className="md:w-14 md:h-14" /></span>
              <span>an application</span>
            </h3>
          </div>
          <div className="lg:basis-1/2">
            <p className="text-base mt-4 font-light">
              with me <span className="text-brand">Aan Sulaiman</span>, a software developer who loves to share how to code in many programing  languages and multi platforms.
            </p>
            <button className="py-2 px-6 bg-brand text-white rounded-full mt-10">Start learn</button>
          </div>
        </section>
        {/* end hero */}

        {/* project */}
        <section className="mt-4">
          <div className="text-center">
            <h3 className="text-lg text-brand">My Project</h3>
          </div>
          <div className="grid grid-cols-1 mt-6 md:grid-cols-2 xl:grid-cols-3 justify-items-center">
            {
              projects.map((item, index) => (
                <div key={index} className="md:mr-3 mt-3">
                  <div className="w-[300px] h-[200px] bg-slate-300 hover:shadow-2xl rounded-lg mb-4">
                    {/* <h1>Hello Project</h1> */}
                  </div>
                  <h4>{item.name}</h4>
                </div>
              ))
            }
          </div>
        </section>
        {/* end project */}

        {/* me */}
        <section className="mt-16">
          <div>
            <h3 className="text-[34px]">
              <span>Its Me</span>
            </h3>
            <h3 className="text-[64px]">
              <span className="font-bold text-brand">Aan Sulaiman</span>
            </h3>

            <p className="max-w-2xl">
              I like to code and keep it simple, try something new, and learn more. I think sharing knowledge is the best practice to improve my skills, and I try to do it.

              I am currently the Chief Technology Officer
              at dSociety, a startup to help students
              prepare for their exams.
            </p>
          </div>
        </section>
        {/* end me */}
      </main>

      <footer className="mt-40 mb-20 w-full py-7 px-4">
        <hr />
        <div className="text-center mt-4 leading-8">
          All Right Reserved @2023 Aan Sulaiman
        </div>
      </footer>
    </>
  )
}
